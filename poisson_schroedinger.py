# Poisson-Schroedinger solver with OO interface
# see poisson_schr3.py for earlier, less flexible non-OO interface.

import scipy as sp
import scipy.constants as cn
import numpy as np
import matplotlib.pyplot as plt
import scipy.sparse
import scipy.sparse.linalg


class poiss_schr:

    def __init__(self):
        self.layers = []

    def addlayer(self, d=0., Ec=0., n=0., x=None):
        if x is not None:
            Ec = .79 * x
        self.layers.append((d, Ec, n))

    def setup(self, qr, T=20, Vbias=(.76, .1), Edx=-.12, epsr=13, meff=.067, h=1, nev=5):

        self.r = np.array(self.layers, dtype=[('d', float), ('Ec', float), ('n', float)])
        r = self.r
        self.qr = qr
        self.h = h
        self.meff = meff
        self.epsr = epsr
        self.Edx = Edx
        # (x-.22)*0.65 according to Fig 7 of Mooney review

        self.setbias(*Vbias)

        self.nev = nev

        self.w = np.sum(r[:]['d'])  # total width of the layer stack

        self.z = np.arange(0, self.w, h)
        self.npts = len(self.z)
        self.Vband = np.zeros(self.npts)
        self.nd = np.zeros(self.npts)

        self.zinter = np.cumsum(r[:]['d'])  # z position of each interface
        for i in range(len(r)):
            mask = (self.z > self.zinter[i]-r[i]['d']) & (self.z <= self.zinter[i])
            self.Vband[mask] = r[i]['Ec']
            self.nd[mask] = r[i]['n']*1e-21  # convert to charges/nm^3

        # todo:
        # check convention for limits - asymmetric here.
        # put limits between points? Extend quantum region?

        # [zmin, zmax] of the QW region
        self.qind = (np.argmin(np.abs(self.z-self.zinter[qr-1])), np.argmin(np.abs(self.z-self.zinter[qr])))
        self.nqu = self.qind[1] - self.qind[0]  # width of the QW region

        # Laplace matrix for quantum problem
        # work in nm and eV
        E0 = .5*(1e9*cn.hbar/h)**2/(cn.e * cn.m_e*meff)
        self.Q0 = E0 * (sp.sparse.diags([2*np.ones(self.nqu), -np.ones(self.nqu-1), -np.ones(self.nqu-1)], [0, 1, -1], format='dok')
                        + sp.sparse.diags(self.Vband[self.qind[0]:self.qind[1]], 0, format='dok'))

        # Laplace for potential
        self.K0 = sp.sparse.diags([2*np.ones(self.npts), -np.ones(self.npts-1), -np.ones(self.npts-1)], [0, 1, -1], format='dok')
        self.K0 /= h**2

        self.settemp(T)
        self.dos = 1e-18 * meff * cn.m_e*cn.e/(np.pi*cn.hbar**2)
        #(states per eV and nm^2)

        self.V = np.zeros(self.npts)

    def settemp(self, T):
        self.beta = cn.e/(cn.k * T)

    def setbias(self, V1, V2):
        self.Vbias = (V1, V2)

    def fermi_dirac(self, E):
        """Compute the Fermi-Dirac distribution at energy E."""
        return 1/(1 + np.exp(-self.beta*E))

    def fermi_dirac_deriv(self, E):
        """Compute the derivative of the Fermi-Dirac distribution at energy E."""
        return self.beta*np.exp(-self.beta*E)*self.fermi_dirac(E)**2

    def solve(self, varion, doquant, maxiter=30, maxerr=1e-10):

        err = 1
        i = 0
        self.nel = np.zeros(self.npts)

        while err > maxerr and i < maxiter:

            Vtot = self.V + self.Vband

            if varion:
                # nd: dopant density
                # nion: ionized dopant density
                E = Vtot + self.Edx
                self.nion = self.nd*self.fermi_dirac(E)
                dnion = sp.sparse.diags(self.nd*self.fermi_dirac_deriv(E), 0, format='dok')
            else:
                dnion = 0.

            if doquant:

                # no band energy in quantum region!
                Q = (self.Q0 + sp.sparse.diags(self.V[self.qind[0]:self.qind[1]], 0, format='dok')).todia()

                self.E, self.psi = sp.sparse.linalg.eigsh(Q, which='SA', k=self.nev, ncv=20)

                # electron density in 1/nm^3 --> nel
                # electron density in 1/nm^2 --> p

                self.p = -np.log(self.fermi_dirac(self.E))*(self.dos/self.beta)
                self.nel[self.qind[0]:self.qind[1]] = (self.psi**2).dot(self.p) / self.h
                # check max occupation

                # energy change + wave function change

                dEp = self.p[None]*(1/(self.E[None] - self.E[:, None] + np.eye(self.nev)) - np.eye(self.nev))
                A = self.psi[None]*self.psi[:, None]

                dnel = sp.sparse.dok_matrix((self.npts, self.npts))
                dnel[self.qind[0]:self.qind[1], self.qind[0]:self.qind[1]] = (
                    - (self.dos/self.h) * ((self.psi**2)*self.fermi_dirac(self.E[None])).dot((self.psi**2).T)
                    + (2/self.h) * np.sum(A.dot(dEp[None]).squeeze()*A, axis=2)
                )

                #((psi*psi.transpose()/(E-E.transpose()))*psi).dot(log(exp(-E*beta)+1)

            else:
                self.nel = 1e-27*(2*self.meff*cn.m_e*cn.e*np.maximum(0, -Vtot)/cn.hbar**2)**1.5/(3*np.pi)
                dnel = sp.sparse.diags(-1e-27*(2*self.meff*cn.m_e*cn.e*np.maximum(0, -Vtot)/cn.hbar**2)**.5/(2*np.pi)
                                       * (2*self.meff*cn.m_e*cn.e*(0 > Vtot)/cn.hbar**2), 0, format='dok')
                self.p = None
                self.E = None
                self.psi = None

            rhs = cn.e / (cn.epsilon_0 * self.epsr)*(self.nel - self.nion)*1e9

            rhs[0] += self.Vbias[0]/self.h**2
            rhs[-1] += self.Vbias[1]/self.h**2

            rf = self.K0.dot(self.V) - rhs
            df = (self.K0 - (cn.e / (cn.epsilon_0 * self.epsr))*(dnel - dnion)*1e9).tocsc()

            self.V -= sp.sparse.linalg.spsolve(df, rf)
            err = np.abs(rf).max()  # does not account for last update
            i += 1

        if i == maxiter:
            print('WARNING: No convergence. err = %.1f after %d iterations.' % (err, i))
        else:
            print('Convergence. err = %.1e after %d iterations.' % (err, i))

        #return V, nel, nion, p, E, psi

    def plotdensity(self, fig=1, xl=200):

        plt.figure(fig)
        plt.clf()
        plt.subplot(211)
        plt.plot(self.z, self.V+self.Vband)
        # plt.xlim(0, xl)
        plt.xlabel('z (nm)')
        plt.title('Conduction band edge (eV)')

        plt.subplot(212)
        #plt.semilogy(z, nion, z, nel)
        plt.plot(self.z, self.nion*1e21, self.z, self.nel*1e21, self.z, self.nd*1e21)

        # plt.xlim(0, xl)
        plt.title('Carrier density (cm$^{-3}$)')
        plt.xlabel('z (nm)')
        plt.legend(['ionized donors', 'electron', 'donor density'])

        plt.tight_layout()

    def plotstates(self, fig=2):
        plt.figure(fig)
        plt.clf()
        plt.subplot(311)
        plt.plot(self.z[self.qind[0]:self.qind[1]], self.V[self.qind[0]:self.qind[1]])
        plt.xlabel('z (nm)')
        plt.title('Potential (eV)')

        plt.subplot(312)
        plt.plot(self.z[self.qind[0]:self.qind[1]], self.psi)
        plt.xlabel('z (nm)')
        plt.title('z-eigenstates')

        plt.subplot(313)
        plt.plot(self.z[self.qind[0]:self.qind[1]], self.nel[self.qind[0]:self.qind[1]])
        plt.title('Electron density (cm$^{-3}$)')
        plt.pause(.001)

        plt.tight_layout()

    def printresults(self):
        print()
        print('----------------------------------------------')
        print('2DEG density: %.2e cm^-2' % (self.p[0]*1e14))
        print('Higher subbands: %.2e cm^-2' % (sum(self.p[1:])*1e14))
        print('E_F %.2f meV' % (-1e3*self.E[0]))
        print('----------------------------------------------')

    def plotcurrent(self, fig=3):
        # plot current density
        psi0 = self.psi[:, 0]
        k = np.sqrt(2*cn.e*(self.E[0]-self.V[self.qind[0]:self.qind[1]])
                    * cn.m_e*self.meff)/cn.hbar*1e-9
        # k in 1/nm, density in 1/nm => Factor 1e18
        j = .5*1e18*cn.hbar/(cn.m_e*self.meff*self.h) * k * \
            np.abs(psi0 + np.gradient(psi0, self.h)/(1j * k))**2

        plt.figure(fig)
        plt.clf()
        plt.subplot(211)
        plt.plot(self.z[self.qind[0]:self.qind[1]], (self.E[0]-self.V[self.qind[0]:self.qind[1]]))

        plt.subplot(212)
        plt.plot(self.z[self.qind[0]:self.qind[1]], np.abs(j))
        plt.xlabel('z (nm)')
        plt.ylabel('Electrons/s')
        plt.title('Current density')

    def tunnel_v2(self, zmax):
        tind = np.argmin(np.abs(self.z-zmax))

        # Tunnel region overlaps with quantum region by 2 samples
        if tind > self.qind[1]:
            tind = [self.qind[1]-2, tind]
        elif tind < self.qind[0]:
            tind = [self.qind[0]+1, tind]
        else:
            print("zmax within quantum region. Aborting.")

        nt = tind[1] - tind[0]

        Vt = (self.V + self.Vband)[tind[0]:tind[1]:np.sign(nt)] - self.E[0]
        E0 = .5*(1e9*cn.hbar/self.h)**2/(cn.e * cn.m_e*self.meff)

        psit = np.zeros(nt)

        if nt > 0:
            psit[0:2] = self.psi[-2:, 0]  # previous also assumed at 0

        for i in range(1, nt-1):
            psit[i+1] = -psit[i-1] + (2.+Vt[i]/E0) * psit[i]

        """
        k = np.sqrt(2*cn.e*Vt[[1,-2]]*cn.m_e*self.meff)/cn.hbar*1e-9

        a1 = (k[0]*psi2[1] + .5j*(psi2[2]-psi2[0])/self.h) \
            /(k[0]*psi1[1] + .5j*(psi1[2]-psi1[0])/self.h)
        print a1


        psi1p = (a1*psi1 + psi2) / abs(a1*psi1[1] + psi2[1])
        """

        plt.figure(3)
        plt.clf()
        plt.subplot(211)
        plt.plot(self.z[tind[0]:tind[1]], Vt)
        plt.subplot(212)
        plt.plot(self.z[tind[0]:tind[1]], psit, self.z[self.qind[0]:self.qind[1]], self.psi[:, 0])

        #plt.figure(4)
        #plt.plot(self.z[tind[0]:tind[1]], np.abs(psi1p))

    def tunnel(self, zmin, zmax):
        tind = (np.argmin(np.abs(self.z-zmin)), np.argmin(np.abs(self.z-zmax)))
        nt = tind[1] - tind[0]

        Vt = (self.V + self.Vband)[tind[0]:tind[1]] - self.E[0]
        E0 = .5*(1e9*cn.hbar/self.h)**2/(cn.e * cn.m_e*self.meff)

        psi1 = np.zeros(nt)
        psi2 = np.zeros(nt)

        psi1[0] = 1.  # previous also assumed at 0
        psi1[1] = 1.
        psi2[-1] = 1.
        psi2[-2] = 1.

        for i in range(1, nt-1):
            psi1[i+1] = -psi1[i-1] + (2.+Vt[i]/E0) * psi1[i]
            psi2[-i-2] = -psi2[-i] + (2.+Vt[-i-1]/E0) * psi2[-i-1]

        k = np.sqrt(-2*cn.e*Vt[[1, -2]]*cn.m_e*self.meff)/cn.hbar*1e-9
        print(k)
        p1l = 1j * k[0] * psi1[1] + .5*(psi1[2]-psi1[0])/self.h
        p2l = 1j * k[0] * psi2[1] + .5*(psi2[2]-psi2[0])/self.h

        p1r = 1j * k[1] * psi1[-2] + .5*(psi1[-1]-psi1[-3])/self.h
        p2r = 1j * k[1] * psi2[-2] + .5*(psi2[-1]-psi2[-3])/self.h

        q1l = 1j * k[0] * psi1[1] - .5*(psi1[2]-psi1[0])/self.h
        q2l = 1j * k[0] * psi2[1] - .5*(psi2[2]-psi2[0])/self.h

        q1r = 1j * k[1] * psi1[-2] - .5*(psi1[-1]-psi1[-3])/self.h
        q2r = 1j * k[1] * psi2[-2] - .5*(psi2[-1]-psi2[-3])/self.h

        # l, r, index indicates transmitted side
        cl = (q2l*p1r-q1l*p2r)/(2j * k[1])
        cr = (q2r*p1l-q1r*p2l)/(2j * k[0])

        dl = (q2l*q1r-q1l*q2r)/(2j * k[1])
        dr = -(q2l*q1r-q1l*q2r)/(2j * k[0])

        # Superpositino solutions with outgoing plane wave
        psil = (q2l*psi1 - q1l*psi2)/cl
        psir = (q2r*psi1 - q1r*psi2)/cr

        tl = (q2l*psi1[1] - q1l*psi2[1])/cl
        tr = (q2r*psi1[-2] - q1r*psi2[-2])/cr

        print('R_L, T_L: ', abs(dl/cl)**2, abs(tl)**2*k[0]/k[1])
        print('1 - R_L: ', 1 - abs(dl/cl)**2)
        print('R_R, T_R: ', abs(dr/cr)**2, abs(tr)**2*k[1]/k[0])
        print('1 - R_R: ', 1 - abs(dr/cr)**2)

        z = self.z[tind[0]:tind[1]]
        plt.figure(3)
        plt.clf()
        plt.subplot(211)
        plt.plot(z, Vt)
        plt.subplot(212)
        plt.plot(z, psi1, z, psi2)

        #plt.plot(self.z[tind[0]:tind[1]], np.abs(psi1p))

        z2 = np.linspace(.0, 2*np.pi/k.max(), 100)
        plt.figure(4)
        plt.clf()
        plt.subplot(211)
        psi = np.exp(1j*k[1] * z2) + dl/cl * np.exp(-1j*k[1]*z2)
        plt.plot(z2+z[-1]-self.h, np.real(psi), z2+z[-1]-self.h, np.imag(psi))
        plt.plot(z, np.real(psil), z, np.imag(psil), z, np.abs(psil))

        plt.subplot(212)
        psi = np.exp(-1j*k[0] * z2) + dr/cr * np.exp(1j*k[0]*z2)
        plt.plot(z[0]-z2+self.h, np.real(psi), z[0]-z2+self.h, np.imag(psi))
        plt.plot(z, np.real(psir), z, np.imag(psir), z, np.abs(psir))

    def run(self, T1=100, T2=4, Tinit=300):
        # generic run

        self.settemp(Tinit)
        print('Coarse run at T = %.0f K...' % Tinit)
        self.solve(True, False, maxerr=1e-4)

        self.settemp(T1)
        print('Solving donor ionization at T = %.0f K...' % T1)
        self.solve(True, False)

        self.settemp(np.sqrt(T1*T2))
        print('Coarse run at T = %.1f K...' % np.sqrt(T1*T2))
        self.solve(False, True, maxerr=1e-2)

        self.settemp(T2)
        print('Solving 2DEG at T = %.1f K...' % T2)
        self.solve(False, True)

        self.printresults()
        self.plotdensity()
        self.plotstates()


""" Missing features:
- plot doping density
- allow switching on bias after freezing dopants.
- smarter initialization: zero with simulated annealin?
- Put less material knowledge into setup (band gap vs x, dx energies built in)



extension of OO interface:
 - setup -> (extension: constructor)
 - addlayer(Ec = , n=, d=,).
     Would need list based layer description -> Future development

"""
