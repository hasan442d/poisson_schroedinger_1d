## example file for poisson_schr5.py, using OO implementation
import poisson_schr5 as ps

# set up structure
str4 = ps.poiss_schr()
 #layer 0; 5nm GaAs
str4.addlayer(5)       
#Layer 1; 35nm of AlGaAs, either you can define offset from conduction band edge...
# or defining the x (Al(x)Ga(1-x)As)  for example: str4.addlayer(35, x=0.3)     
str4.addlayer(35, Ec=.27)     
# Layer 3; another Layer with Ec=0.27 and dopant concentration of n=9e18
str4.addlayer(10, Ec=.27)
#layer 4; 50nm of GaAs, we will solve the Schrodinger equ. in this layer.
str4.addlayer(50) 
#Layer 5; 1000nm of AlGaAs(Ec=0.27)  
str4.addlayer(1000, Ec=.27)

qreg = 4 # defines in which layer we want to solve the schrodinger equ. (Here layer 4); 

# set remaining parameters
str4.setup(qreg, Edx = -.12)

# do a full simulation runs. It also possile to execute different steps 
# individually, as done below 
str4.run()

#%% Solve again with different bias conditions.
str4.setbias(1.4, -5.5)
str4.solve(False, True)
str4.plotdensity(11)
str4.plotstates(12)
str4.printresults()
#%%
str4.tunnel(105, 180)
str4.plotcurrent()

#%% Same with simulated bias cooling. 
# Dopants are simulated at 100 K and then frozen.
# The initial run at 300 K is only there to help convergence.

str4.setbias(.5, .1)
str4.settemp(300)
str4.solve(True, False, maxerr = 1e-4)
str4.settemp(100)
str4.solve(True, False)

# More bias would cause leakage
str4.setbias(.3, .1)
str4.solve(True, False)

str4.plotdensity(1)

#%% 2DEG with slightly reduced bias
str4.settemp(4)
str4.setbias(.1, .1)
str4.solve(False, False)
str4.solve(False, True)

str4.plotdensity(3)
str4.printresults()
str4.plotstates(2)


#%% Operating bias
str4.setbias(.7, -5.5)
str4.solve(False, True)

str4.printresults()
str4.plotdensity(11)
str4.plotstates(12)

#%%
str4.tunnel(105, 180)
str4.plotcurrent()



#%% example code for comparison of different structures
slist = [str4, str5, str6]
slist = [str4, str5]

figure(3)
clf()
subplot(211)
for s in slist:
    plot(s.z, s.V+s.Vband)
xlim(0, 200)
ylabel('Ec (eV)')

subplot(212)
for s in slist:
    plot(s.z, s.nion)
xlim(0, 200)

xlabel('z (nm)')
ylabel('ionized donor density (cm^-3)')
legend(['near delta-doped', '15 nm modulation doped', '40 nm modulation doped'])



