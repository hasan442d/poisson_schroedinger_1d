## example file for poisson_schr5.py, using OO implementation

import poisson_schr5 as ps

# set up structure
str4 = ps.poiss_schr()
 #layer 0; 5nm GaAs
str4.addlayer(5)       
#Layer 1; 35nm of AlGaAs, either you can define offset from conduction band edge...
# or defining the x (Al(x)Ga(1-x)As)  for example: str4.addlayer(35, x=0.3)     
str4.addlayer(35, Ec=.27) 
#layer2
str4.addlayer(5, Ec=.27, n=9e18)    
# Layer 3; another Layer with Ec=0.27 and dopant concentration of n=9e18
str4.addlayer(10, Ec=.27)
#layer 4; 50nm of GaAs, we will solve the Schrodinger equ. in this layer.
str4.addlayer(50) 
#Layer 5; 1000nm of AlGaAs(Ec=0.27)  
str4.addlayer(1000, Ec=.27)

qreg = 4 # defines quantum region; The layer, in which we want to solve the schrodinger equ. (Here layer 4); 

# set remaining parameters
str4.setup(qreg, Edx = -.12)

# do a full simulation runs. It also possile to execute different steps 
# individually, as done below 
#The "run" procedure is as follows: (This leads to a better convergence)
# applieds the predefined voltage bias to the structure (0.76 volt)
# first solving the poisson equ. at 300 K
# solving the  the poisson equ. at 100 K
# the configuration of ionized dopants will be kept constant for next simulations.
# solve the self consistant equ. at 20 K and then solving at 4 K
  
str4.run()
#%% Solving with different Tempreture
str4.settemp(T=20)
#with the above instruction you set the temperature;
str4.solve(False, True) # solves the structure...
# if the first argumant is False this means that the ions are frozen and the sover uses the previous results for ion configuration
# when thge second argument is True means that the solver, solves the schrodinger equ. in the quantum region
# and calculates the ground state and other excited states.
str4.printresults()
str4.plotdensity()
str4.plotstates()
    

 

#%% Solve again with different bias conditions.
str4.setbias(1.37, 0.1)  # applies -1.4 Volt to the structure
# we can set the bias conditions by using "Varname.setbias(V1,V2)" -V1 is...
# voltage applied to the left side of the structure and  V2 is the potential deep inside the structure...
# It does not play a big role and you can leave it fixed.
# Note that with above instruction you will change the conduction band edge at left side of...
# the structure which this has a negative sign diffrence with thw acctual electric voltage applied to the structure.  
str4.settemp(T=4)
str4.solve(False, True)
str4.plotdensity(11)
str4.plotstates(12)
str4.printresults()

#%% Same with simulated bias cooling. 
# Dopants are simulated at 100 K and then frozen.
# The initial run at 300 K is only there to help convergence.

str4.setbias(.5, .1)
str4.settemp(300)
str4.solve(True, False, maxerr = 1e-4)
str4.settemp(100)
str4.solve(True, False)

# More bias would cause leakage
str4.setbias(0.5, .1)
str4.solve(True, False)

str4.plotdensity(1)

